import React, { useEffect, useState } from 'react';
import { useAuth } from '../src/hooks/useAuth';
import { ApiResponse } from '../src/data/ApiResponse';
import Image from 'next/image';
import styles from '../styles/Profilepage.module.scss';
import { FaBaby } from 'react-icons/fa';
import { FiLogOut } from 'react-icons/fi';
import { GiMilkCarton, GiLungs } from 'react-icons/gi';

interface User {
    userId: string;
    email: string;
    firstName: string;
    lastName: string;
    dateOfBirth: Date;
}

interface Label {
    label: string;
    value: number;
    icon: JSX.Element;
}

interface Policy {
    policyCode: string;
    items: {
        breastCancerRisk: Label;
        lungCancerRisk: Label;
        cervicalCancerRisk: Label;
    };
}
export default function Profile() {
    const {token, logout} = useAuth();

    const [userData, setUserData] = useState<User>({
        userId: '',
        email: '',
        firstName: '',
        lastName: '',
        dateOfBirth: new Date(),
    });

    const [policyDetails, setPolicyDetails] = useState<Policy>({
        policyCode: '',
        items: {
            breastCancerRisk: {
                label: '',
                value: 0,
                icon: <GiMilkCarton size={30} />,
            },
            lungCancerRisk: {
                label: '',
                value: 0,
                icon: <GiLungs size={30} />,
            },
            cervicalCancerRisk: {
                label: '',
                value: 0,
                icon: <FaBaby size={30} />,
            },
        },
    });
    useEffect(() => {
        const fetchUserDetails = async () => {
            const res = await fetch(`${process.env.SERVER_URL}/users`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            const data: ApiResponse.User = await res.json();
            setUserData({
                userId: data.userId,
                email: data.email,
                firstName: data.firstName,
                lastName: data.lastName,
                dateOfBirth: new Date(data.dateOfBirth),
            });
        };
        const fetchGeneticsResults = async () => {
            const res = await fetch(`${process.env.SERVER_URL}/users/genetics-results`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            const data: ApiResponse.GeneticsResults = await res.json();
            setPolicyDetails((state) => ({
                policyCode: data.policyCode,
                items: {
                    breastCancerRisk: {
                        label: 'Breast Cancer Risks',
                        value: parseInt(data.breastCancerRisk),
                        icon: state.items.breastCancerRisk.icon,
                    },
                    lungCancerRisk: {
                        label: 'Lung Cancer Risks',
                        value: parseInt(data.lungCancerRisk),
                        icon: state.items.lungCancerRisk.icon,
                    },
                    cervicalCancerRisk: {
                        label: 'Cervial Cancer Risks',
                        value: parseInt(data.cervicalCancerRisk),
                        icon: state.items.cervicalCancerRisk.icon,
                    },
                },
            }));
        };
        fetchUserDetails();
        fetchGeneticsResults();
    }, [token]);


    return (
        <div className={styles.container}>
            <main className={styles.main}>
                <div className={styles.header}>
                    <div className={styles.banner}>
                        <Image src="/wall.png" layout="responsive" width={100} height={25} objectFit="cover" />
                    </div>
                    <div className={styles.profile}>
                        <div className={styles.profileContainer}>
                            <div className={styles.profilePic}>
                                <Image src="/download.jpeg" layout="responsive" width={10} height={10} />
                            </div>
                            <div className={styles.profileInfo}>
                                <div className={styles.name}>{`${userData.firstName} ${userData.lastName}`}</div>
                                <div className={styles.info}>Date of birth: {userData.dateOfBirth.toDateString()}</div>
                                <div className={styles.info}>Email: {userData.email}</div>
                                <div className={styles.info}>User Id: {userData.userId}</div>
                            </div>
                        </div>
                        <div className={styles.logout} onClick={()=>logout()}>
                            <FiLogOut />
                        </div>
                    </div>
                </div>
                <div className={styles.report}>
                    <div className={styles.reportTitle}>Your Cancer Risks</div>
                    <div className={styles.reportPolicy}>Policy code: {policyDetails.policyCode}</div>
                    <div>
                        {Object.keys(policyDetails.items).map((risk) => (
                            <div className={styles.reportDetails}>
                                <div className={styles.riskItem}>
                                    <div>{policyDetails.items[risk].icon}</div>
                                    <div className={styles.label}>{policyDetails.items[risk].label}</div>

                                </div>
                                <div className={styles.bar}>
                                    <div
                                        className={styles.percent}
                                        style={{ width: policyDetails.items[risk].value }}
                                    ></div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </main>
        </div>
    );
}
