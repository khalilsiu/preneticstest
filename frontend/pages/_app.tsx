import '../styles/globals.scss';
import { AppProps } from 'next/app';
import React from 'react';
import Head from 'next/head';
// import { AnimatePresence } from 'framer-motion';

function MyApp({ Component, pageProps, router }: AppProps) {
    return (
        <>
            <Head>  
                <link href="https://fonts.googleapis.com/css2?family=Playfair+Display&display=swap" rel="stylesheet" />
                <title>🔥 Prenetics | Your health partner</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            {/* <AnimatePresence initial={false} exitBeforeEnter> */}
                <Component {...pageProps}  />
            {/* </AnimatePresence> */}
        </>
    );
}

export default MyApp;
