import styles from '../styles/Home.module.scss';
import { useForm } from 'react-hook-form';
import { useRouter } from 'next/dist/client/router';

export default function Home() {
    const { register, handleSubmit, errors, watch } = useForm<{
        email: string;
        password: string;
    }>();

    const router = useRouter();

    const onSubmit = async ({ email, password }) => {
        console.log(`${process.env.SERVER_URL}/users/login`);
        const res = await fetch(`${process.env.SERVER_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email,
                password,
            }),
        });
        const { token } = await res.json();
        localStorage.setItem('token', token);
        router.push('/profile');
    };

    return (
        <div className={styles.container}>
            <main className={styles.main}>
                <div className={styles.loginText}>Sign In.</div>
                <div className={styles.instructions}>Please sign in with your email.</div>
                <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
                    <input
                        ref={register({
                            required: true,
                            pattern: {
                                value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                message: 'Input must be a valid email.',
                            },
                        })}
                        aria-label="email"
                        name="email"
                        type="email"
                        required
                        className={styles.inputBox}
                        placeholder="Email"
                    />
                   {errors.email && (
                        <span className={styles.errorMessage}>
                            {errors.email.message}
                        </span>
                    )}
                    <input
                        ref={register({
                            required: true,
                            minLength: {
                                value: 8,
                                message: 'Password should be more than 8 characters long',
                            },
                        })}
                        aria-label="Password"
                        name="password"
                        type="password"
                        required
                        className={styles.inputBox}
                        placeholder="Password"
                    />
                    {errors.password && (
                        <span className={styles.errorMessage}>
                            {errors.password.message}
                        </span>
                    )}
                    <button
                        type="submit"
                        className={styles.loginButton}
                        
                    >Sign In</button>
                </form>
            </main>

            <footer className={styles.footer}></footer>
        </div>
    );
}
