import { useEffect, useState } from 'react';
import { useRouter } from 'next/dist/client/router';

export function useAuth() {
    const router = useRouter();
    const [token, setToken] = useState('');
    useEffect(() => {
        const token = localStorage.getItem('token');
        console.log('token', token)
        if (!token) {
            router.push('/');
        } else {
            setToken(token);
        }
    }, []);

    function logout(){
        localStorage.removeItem('token');
        console.log('logout')
        router.push('/')
    }

    return {token, logout};
}
