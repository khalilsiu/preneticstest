export namespace ApiResponse {
    export interface User {
        userId: string;
        email: string;
        firstName: string;
        lastName: string;
        dateOfBirth: string;
    }

    export interface GeneticsResults {
        userId: string;
        policyCode: string;
        breastCancerRisk: string;
        lungCancerRisk: string;
        cervicalCancerRisk: string;
    }
}
