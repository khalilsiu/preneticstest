import * as bcrypt from 'bcrypt';

const SALT_ROUNDS = 10;

export default class Password {
    static async hashPassword(plainPassword: string) {
        const hash = await bcrypt.hash(plainPassword, SALT_ROUNDS);
        return hash;
    }

    static async checkPassword(plainPassword: string, hashPassword: string) {
        const match = await bcrypt.compare(plainPassword, hashPassword);
        return match;
    }
}
