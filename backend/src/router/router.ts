import * as express from 'express';
import userRouter from '../user/infra/routes';

const mainRouter = express.Router();


mainRouter.use('/users', userRouter);

export default  mainRouter 
