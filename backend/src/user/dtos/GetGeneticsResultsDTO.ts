export default interface GetGeneticsResultsDTO {
    user_id: string;
    policyCode: string;
    breastCancerRisk: number;
    lungCancerRisk: number;
    cervicalCancerRisk: number;
}