export default interface UserDTO {
    id: string;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    dateOfBirth: Date;

}