import { User } from '../domain/User';
import UserDTO from '../dtos/UserDTO';

export class UserMap {

    public static toDomain(dto: UserDTO): User {
        try {
            const user = User.create({
                userId: dto.id,
                email: dto.email,
                password: dto.password,
                firstName: dto.firstName,
                lastName: dto.lastName,
                dateOfBirth: dto.dateOfBirth
            });
            return user;
        } catch (err) {
            throw new Error(err);
        }
    }
}
