import { GeneticsResults } from '../domain/GeneticsResults';
import GetGeneticsResultsDTO from '../dtos/GetGeneticsResultsDTO';

export class GeneticsResultsMap {

    public static toDomain(dto: GetGeneticsResultsDTO): GeneticsResults {
        try {
            const result = GeneticsResults.create({
               userId: dto.user_id,
               policyCode: dto.policyCode,
               breastCancerRisk:  dto.breastCancerRisk,
               lungCancerRisk: dto.lungCancerRisk,
               cervicalCancerRisk:  dto.cervicalCancerRisk,
            });
            return result;
        } catch (err) {
            throw new Error(err);
        }
    }

}
