import { IGeneticsResultsRepo } from '../../repo/GeneticsResultsRepo';
import GetGeneticsResultsByUserIdDTO from '../../dtos/GetGeneticsResultsByUserIdDTO';

export default class GetGeneticsResultsByUserId {
    private geneticsResultsRepo: IGeneticsResultsRepo;
    constructor(geneticsResultsRepo: IGeneticsResultsRepo) {
        this.geneticsResultsRepo = geneticsResultsRepo;
    }

    public async execute(request: GetGeneticsResultsByUserIdDTO) {
        try {
            const { userId } = request;
            const entry = await this.geneticsResultsRepo.getGeneticsResultsByUserId(userId);
            const result = {
                userId: entry.userId,
                policyCode: entry.policyCode,
                breastCancerRisk: entry.breastCancerRisk,
                lungCancerRisk: entry.lungCancerRisk,
                cervicalCancerRisk: entry.cervicalCancerRisk,
            };
            return result;
        } catch (err) {
            throw new Error(err);
        }
    }
}
