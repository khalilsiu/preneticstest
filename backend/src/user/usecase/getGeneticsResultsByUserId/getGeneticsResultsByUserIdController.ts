
import { Request, Response, NextFunction } from 'express';
import GetGeneticsResultsByUserId from './getGeneticsResultsByUserId';
import UserPayload from '../../dtos/UserPayload'


export default class GetGeneticsResultsByUserIdController {
    constructor(private usecase: GetGeneticsResultsByUserId) {
        this.usecase = usecase;
    }

    public async execute(req: Request, res: Response, next: NextFunction) {
        try{
            const {userId}  = req.user as UserPayload;
            const result = await this.usecase.execute({ userId });
            res.status(200).send(result)
        }catch (err){
            next(err);
        }

    }
}
