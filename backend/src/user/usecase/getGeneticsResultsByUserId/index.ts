

import PsqlGeneticsResultsRepo from '../../repo/psqlRepo/PsqlGeneticsResultsRepo';
import {knex} from '../../../knex'
import GetGeneticsResultsByUserId from './getGeneticsResultsByUserId';
import GetGeneticsResultsByUserIdController from './getGeneticsResultsByUserIdController';


const geneticsRepo = new PsqlGeneticsResultsRepo(knex);
const getGeneticsResultsByUserId = new GetGeneticsResultsByUserId(geneticsRepo);
const getGeneticsResultsByUserIdController = new GetGeneticsResultsByUserIdController(getGeneticsResultsByUserId);

export default getGeneticsResultsByUserIdController;
