import Login from './login';
import { Request, Response, NextFunction } from 'express';

export default class LoginController {
    constructor(private usecase: Login) {
        this.usecase = usecase;
    }

    public async execute(req: Request, res: Response, next: NextFunction) {
        try{
            const { email, password } = req.body;
            const result = await this.usecase.execute({ email, password });
            res.json({token: result})
        }catch (err){
            next(err);
        }

    }
}
