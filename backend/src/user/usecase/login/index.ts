

import PsqlUserRepo from '../../repo/psqlRepo/PsqlUserRepo';
import Login from './login';
import LoginController from './loginController';
import {knex} from '../../../knex'


const userRepo = new PsqlUserRepo(knex);
const loginUseCase = new Login(userRepo);
const loginController = new LoginController(loginUseCase);

export default loginController;
