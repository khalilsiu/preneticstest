
import Password from '../../../shared/hash';
import { User } from '../../domain/User';
import LoginDTO from '../../dtos/LoginDTO';
import { IUserRepo } from '../../repo/UserRepo';
import * as jwtSimple from 'jwt-simple'
import jwt from '../../../jwt';


export default class Login {
    private userRepo: IUserRepo;
    constructor(userRepo: IUserRepo) {
        this.userRepo = userRepo;
    }

    public async execute(request: LoginDTO) {
        try {
            const { email, password } = request;
            let user: User;
            try {
                user = await this.userRepo.getUserByEmail(email);
            } catch (err) {
                throw new Error(`User ${err}`);
            }
            if (! await Password.checkPassword(password,user.password)){
                throw new Error('Wrong password');
            }

            const payload = {
                userId: user.userId,
                email: user.email,
                firstName: user.firstName,
                lastName: user.lastName,
            }
            return jwtSimple.encode(payload, jwt.jwtSecret);
        } catch (err) {
            throw new Error(err)
        }
    }
}
