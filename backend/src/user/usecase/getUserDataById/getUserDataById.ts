
import { IUserRepo } from '../../repo/UserRepo';
import GetUserDataByIdDTO from '../../dtos/GetUserDataByIdDTO';


export default class GetUserDataById {
    private userRepo: IUserRepo;
    constructor(userRepo: IUserRepo) {
        this.userRepo = userRepo;
    }

    public async execute(request: GetUserDataByIdDTO) {
        try {
            const { userId } = request
            const user = await this.userRepo.getUserByUserId(userId);
            const result = {
                userId: user.userId,
                email: user.email,
                firstName: user.firstName,
                lastName: user.lastName,
                dateOfBirth: user.dateOfBirth
            }
            return result;
        } catch (err) {
            throw new Error(err)
        }
    }
}
