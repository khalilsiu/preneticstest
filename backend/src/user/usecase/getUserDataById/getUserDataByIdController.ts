import { Request, Response, NextFunction } from 'express';
import GetUserDataById from './getUserDataById';
import UserPayload from '../../dtos/UserPayload'

export default class GetUserDataController {
    constructor(private usecase: GetUserDataById) {
        this.usecase = usecase;
    }

    public async execute(req: Request, res: Response, next: NextFunction) {
        try {
            const { userId } = req.user as UserPayload;
            const result = await this.usecase.execute({ userId });
            res.status(200).send(result);
        } catch (err) {
            next(err);
        }
    }
}
