

import PsqlUserRepo from '../../repo/psqlRepo/PsqlUserRepo';
import {knex} from '../../../knex'
import GetUserDataById from './getUserDataById';
import GetUserDataController from './getUserDataByIdController';


const userRepo = new PsqlUserRepo(knex);
const getUserDataById = new GetUserDataById(userRepo);
const getUserDataController = new GetUserDataController(getUserDataById);

export default getUserDataController;
