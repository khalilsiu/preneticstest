
import * as Knex from 'knex';
import { IGeneticsResultsRepo } from '../GeneticsResultsRepo';
import { GeneticsResults } from '../../domain/GeneticsResults';
import { GeneticsResultsMap } from '../../mapper/GeneticsResultsMap';

export default class PsqlGeneticsResultsRepo implements IGeneticsResultsRepo {
    private knex: Knex;

    constructor(knex: Knex) {
        this.knex = knex;
    }

   async getGeneticsResultsByUserId(userId: string): Promise<GeneticsResults> {
        const result = await this.knex.select('*').from('policies').where('user_id', userId).first();
        if (!result) {
            throw new Error('Result not found')
        }
        return GeneticsResultsMap.toDomain(result)
   }


}
