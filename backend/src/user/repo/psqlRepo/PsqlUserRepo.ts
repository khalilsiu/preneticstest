import { IUserRepo } from '../UserRepo';
import * as Knex from 'knex';
import { User } from '../../domain/User';
import { UserMap } from '../../mapper/UserMap';
import UserDTO from '../../dtos/UserDTO';

export default class PsqlUserRepo implements IUserRepo {
    private knex: Knex;

    constructor(knex: Knex) {
        this.knex = knex;
    }

    async getUserByEmail(email: string): Promise<User> {
        const userEntry: UserDTO= await this.knex.select('*').from('users').where('email', email).first()
        if (!userEntry){
            throw new Error('User not found');
        }
        return UserMap.toDomain(userEntry)

    }

    async getUserByUserId(userId: string): Promise<User> {
        const userEntry: UserDTO= await this.knex.select('*').from('users').where('id', userId).first()
        if (!userEntry){
            throw new Error('User not found');
        }
        return UserMap.toDomain(userEntry)

    }
}
