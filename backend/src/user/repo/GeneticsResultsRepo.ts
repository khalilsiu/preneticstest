import { GeneticsResults } from "../domain/GeneticsResults";


export interface IGeneticsResultsRepo {
    getGeneticsResultsByUserId(userId: string): Promise<GeneticsResults>
}
