import { User } from "../domain/User";


export interface IUserRepo {
      getUserByEmail(userEmail: string): Promise<User>;
      getUserByUserId(userId: string): Promise<User>;
}
