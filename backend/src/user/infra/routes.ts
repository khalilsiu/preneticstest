import * as express from 'express';
import loginController from '../usecase/login';
import getUserDataByIdController from '../usecase/getUserDataById';
import getGeneticsResultsByUserIdController from '../usecase/getGeneticsResultsByUserId';
import { isLoggedIn } from '../../guard';

const userRouter = express.Router();

userRouter.post('/login', (req, res, next) => loginController.execute(req, res, next));
userRouter.get('/', isLoggedIn, (req, res, next) => getUserDataByIdController.execute(req, res, next));
userRouter.get('/genetics-results', isLoggedIn, (req, res, next) => getGeneticsResultsByUserIdController.execute(req, res, next));

export default userRouter;
