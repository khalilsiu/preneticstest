import { Guard } from "../../shared/Guard";

export interface GeneticsResultsProps {
    userId: string;
    policyCode: string;
    breastCancerRisk: number;
    lungCancerRisk: number;
    cervicalCancerRisk: number;

  }

export class GeneticsResults {
  get userId(): string {
    return this.props.userId;
  }

  get policyCode(): string {
    return this.props.policyCode;
  }

  get breastCancerRisk(): number {
    return this.props.breastCancerRisk;
  }

  get lungCancerRisk(): number {
    return this.props.lungCancerRisk;
  }

  get cervicalCancerRisk(): number {
    return this.props.cervicalCancerRisk;
  }

  private props: GeneticsResultsProps;

  private constructor(props: GeneticsResultsProps) {
    this.props = props;
  }

  private static isValidPolicyCode(policyCode: string) { 
    return policyCode.length === 8;
  }

  public static create(props: GeneticsResultsProps): GeneticsResults {
    if (!this.isValidPolicyCode(props.policyCode)){
        throw new Error('Policy code should be an 8 alphanumeric code');
    }
    const guardResult = Guard.againstNullOrUndefinedBulk([
      { argument: props.userId, argumentName: 'userId' },
      { argument: props.policyCode, argumentName: 'policyCode' },
      { argument: props.breastCancerRisk, argumentName: 'breastCancerRisk' },
      { argument: props.lungCancerRisk, argumentName: 'lungCancerRisk' },
      { argument: props.cervicalCancerRisk, argumentName: 'cervicalCancerRisk' },
    ]);

    if (!guardResult.succeeded) {
      throw new Error(guardResult.message);
    }

    return new GeneticsResults(props)
  }
}
