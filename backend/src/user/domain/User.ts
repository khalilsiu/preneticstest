import { Guard } from "../../shared/Guard";

export interface UserProps {
    userId: string;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    dateOfBirth: Date;
  }

export class User {
  get userId(): string {
    return this.props.userId;
  }

  get email(): string {
    return this.props.email;
  }

  get password(): string {
    return this.props.password;
  }

  get firstName(): string {
    return this.props.firstName;
  }

  get lastName(): string {
    return this.props.lastName;
  }

  get dateOfBirth(): Date {
    return this.props.dateOfBirth;
  }

  private props: UserProps;

  private constructor(props: UserProps) {
    this.props = props;
  }

  private static isValidEmail(email: string) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  public static create(props: UserProps): User {
    if (!this.isValidEmail(props.email)){
        throw new Error('Email is invalid');
    }
    const guardResult = Guard.againstNullOrUndefinedBulk([
      { argument: props.email, argumentName: 'email' },
      { argument: props.password, argumentName: 'password' },
      { argument: props.firstName, argumentName: 'firstName' },
      { argument: props.lastName, argumentName: 'lastName' },
      { argument: props.dateOfBirth, argumentName: 'dateOfBirth' },
    ]);

    if (!guardResult.succeeded) {
      throw new Error(guardResult.message);
    }

    return new User(props)
  }
}
