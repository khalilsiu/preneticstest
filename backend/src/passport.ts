import * as passport from 'passport';
import * as passportJWT from 'passport-jwt';
import jwt from './jwt';
import PsqlUserRepo from './user/repo/psqlRepo/PsqlUserRepo';
import { knex } from './knex'

const JWTStrategy = passportJWT.Strategy;
const { ExtractJwt } = passportJWT;
const userRepo = new PsqlUserRepo(knex);

passport.use(
    new JWTStrategy(
        {
            secretOrKey: jwt.jwtSecret,
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        },
        async (payload, done) => {
            const user = await userRepo.getUserByEmail(payload.email);
            if (user) {
                return done(null, payload);
            } else {
                return done(new Error('User not Found'), null);
            }
        },
    ),
);
