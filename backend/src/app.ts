import * as express from 'express';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import * as passport from 'passport';
import * as expressSession from 'express-session';
import { Request, Response, NextFunction } from 'express';
import mainRouter from './router/router';

const sessionMiddleware = expressSession({
    secret: 'prenetics health reimagined',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false },
});
const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyParser.json());

app.use(sessionMiddleware);
app.use(passport.initialize());
import './passport';
app.use('/', mainRouter);

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    res.status(500).send({ error: err.message });
});

const PORT = 38080;

app.listen(PORT, () => console.log('listening to port: ', PORT));
