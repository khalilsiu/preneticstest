import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("policies").del();
    await knex("users").del();

    // Inserts seed entries
    const data = await knex("users").insert([
        {email: 'peterchan@gmail.com', password: '$2b$10$vS7T.74.RMN8GeSctAsAqO.jVGk2skS85DQljju0nnJeBuDdaPcUu', firstName: 'peter', lastName: 'chan', dateOfBirth: new Date('1967-02-23')},
        {email: 'sophiafan@gmail.com', password: '$2b$10$vS7T.74.RMN8GeSctAsAqO.jVGk2skS85DQljju0nnJeBuDdaPcUu', firstName: 'sophia', lastName: 'fan',dateOfBirth: new Date('1942-12-06')},
        {email: 'nathanlee@gmail.com', password: '$2b$10$vS7T.74.RMN8GeSctAsAqO.jVGk2skS85DQljju0nnJeBuDdaPcUu', firstName: 'nathan', lastName: 'lee',dateOfBirth: new Date('1988-07-12')},
    ]).returning('id');

    // Inserts seed entries
    await knex("policies").insert([
        {user_id: data[0], policyCode: 'JK9NE3PD', breastCancerRisk: 0.33, lungCancerRisk: 3.42, cervicalCancerRisk: 15.32},
        {user_id: data[1], policyCode: 'K3NWPD02', breastCancerRisk: 10.23, lungCancerRisk: 5.62, cervicalCancerRisk: 14.33},
        {user_id: data[2], policyCode: 'R903KDN6', breastCancerRisk: 6.55, lungCancerRisk: 7.43, cervicalCancerRisk: 3.03},
    ]);
};
