import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('policies');
    if (!hasTable) {
        return knex.schema.createTable('policies', (table) => {
            table.increments();
            table.integer('user_id').unsigned().notNullable()
            table.foreign('user_id').references('users.id')
            table.string('policyCode', 8).notNullable();
            table.decimal('breastCancerRisk', 15, 2);
            table.decimal('lungCancerRisk', 15, 2);
            table.decimal('cervicalCancerRisk', 15, 2);
            table.timestamps(true,true);
        });
    } else {
        return Promise.resolve();
    }
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('policies')
}
