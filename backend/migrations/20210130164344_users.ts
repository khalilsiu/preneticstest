import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('users');
    if (!hasTable) {
        return knex.schema.createTable('users', (table) => {
            table.increments();
            table.string('email').notNullable();
            table.string('password').notNullable();
            table.string('firstName').notNullable();
            table.string('lastName').notNullable();
            table.date('dateOfBirth').notNullable()
            table.timestamps(true, true);
        });
    } else {
        return Promise.resolve();
    }
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('users')
}
