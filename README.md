# Prenetics test

This repository contains code for the test app for Prenetics test


# Setup

The backend of this service runs on PostgreSQL, therefore please install it and create a database call prudential test.

`cd backend`
In backend/ please create a .env file with the following:

DB_NAME=preneticstest

DB_USERNAME=yourUserName

DB_PASSWORD=yourPassword

After that, please run `npm install` to install all the dependencies
Run `npm run migrate:latest` to migrate to the latest table structure
Run `npm run seed:run` to input all the seed data to the database
Run `npm run local` to spin up the local server 

`cd frontend`
The frontend is a NextJs application, run `yarn` to install all the dependencies.
Run `yarn run dev` to start the development app for the frontend.

You can login with the seed data

email: peterchan@gmail.com

password: 12345678

# Database design
Database design

The database contains 2 tables, namely the User table and the Policies table. The Users table contains all the information related to the user including the first name (varchar), last name (varchar), email address (varchar), password hash (varchar) and date of birth (date), while the Policies table contain all policy related data including policyCode(varchar) and other data e.g. cancerRisks(decimal). Currently both tables mostly contain read related data, if there contains more write related data e.g. "likes", then it should be separated from read data as a separated table to reduce the load of requests to the master node. The structure of the database could be structured to have 1 master node which handles both read and write operations with multiple slave nodes that only perform read operations. This allows the separation of read and write of the database to lower the load to the database server, while also provide a back up when any of the database servers is down. 

API design

Funtional

The backend is a minified Domain Driven Design where the structure has broken down into small use cases e.g. getGeneticsResultsByUserId. While the name might sound long, but this gives context to the domain experts as well as the engineers who read it a common ground to understand the structure better.
This structure also complies with SOLID principle which makes unit testing, switching to another database, extending the API functionalities much easier as it emphasizes coding to interfaces instead of implementations.

Security

This app uses a simple bearer token for authentication, it is simple and fast yet it compromises on security as it only relies on one key which might prone to compromisation when someone gets a hand on it. It would be much secure to have 2 token mechanism which the access token that grants access to restrictive resources has an expiry date while a long-lived refresh token could be used to obtain the access token.

Validation

Domain objects (objects that are created, located in /src/user/domain) contain validation functions which the objects could be created only when the validation functions return true e.g. User, it has static function to validate an email entry, creating the object also validates if all items required are being provided. The creation of the domain object contains logic to invalidate null or undefined parameters to prevent entries being created unexpectedly.

Concurrency

Currently the server does not handle concurrency as the app only contain read operations and the expected traffic is low, however for extensive concurrent write operations, we should use optimistic locking where an update request is only successful if the resource has not been modified since the client last checked. The client application gets the resource that it wishes to update and stores the ETag that the server returns. It performs any relevant updates and POSTs the resource back with the If-Match condition header together with the stored Etag. If the ETag matches the resource’s current ETag, the server performs that update and responds with success. Otherwise, the server rejects the update with a 412 Precondition Failed response.

Scale up and down API and database for variable traffic

A load balancer with the use of multiple server will help when traffic is increasing. It will redirect traffic to servers that have capacity without the need to scale up the servers. Spreading servers across multiple regions also helps maintaining the server availabilities. Alternatively, a Pod autoscaler could be used in kubernetes to regulate the number of active nodes in the cluster. A Horizontal Pod Autoscaler controller monitors the workload's pod metrics such as CPU usage, network traffic, memory or other values, and it would auto regulate by comparing the current value with the target value. To configure a Pod Autoscaler controller, you would have to configure the HPA controller using a manifest

```
apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  name: example-app
  namespace: default
spec:
  maxReplicas: 10
  minReplicas: 1
  scaleTargetRef:
    apiVersion: extensions/v1beta1
    kind: Deployment
    name: example-app
  targetCPUUtilizationPercentage: 50
```

Testing strategy

Unit tests should be run to ensure that components of the server and the frontend could deliver the expected output upon a given input. Integration tests should also be run to ensure that connecting services across modules and the interaction with the database are working as expected. End to end test should be run to ensure that the user flows work as expected starting from the user interface all the way to database back to the client. The above three tests are to be automated and incorporated inside the CICD pipeline to speed up the feedback loop so that errors and bugs are detected early before pushing to the testing environment to be tested by a human. A performance test should also be carried out to measure the reliability, stability and availbaility of the deployed application to observe the change in response time under a high number of requests.
